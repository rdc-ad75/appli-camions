# AppliCamions

Projet de migration de l'application Camion75 actuellement en production (Symfony 3.4/Bootstrap) pour la gestion des rapports et communications pour les distribution camions
Ce projet vise à migrer cette application en utilisant le framewok Django.
Les raisons qui poussent cette migration sont que :
* nous n'avons pas trouvé de bénévoles compétents et intéressés par ce framework malgré sa bonne rénommée
* le fait que ce soit un framework php semble dissuader les développeurs avertis qui sont plus orientés vers des langages plus robustes et framework libres (ruby, django, ...)
* l'application actuelle étant en Symfony 3.4 nécessite une mise à jour conséquente et tant qu'à la refaire autant au profiter pour essayer une autre structure

# Récupération du dépôt

* Installer l'utilitaire Git
* Exécuter la commande ```git clone git@gitlab.com:rdc-ad75/appli-camions.git```

# Installation du projet

Installation des paquages python de base de projet
```pip install -r requirements/base.txt```

Puis installation des paquages python selon l'environnement souhaité dev, test, ou prod
```
# dev
pip install -r requirements/dev.txt
# ou test
pip install -r requirements/dev.txt
# ou prod
pip install -r requirements/dev.txt
```


# Gestion des paramètres de l'applications selon environnement dev/test/prod

La gestion des paramètres est inspirée de la [gestion fichiers de configuration du framework Symfony](https://symfony.com/doc/current/configuration.html)
C'est la variable APP_ENV qui définit l'environnement d'exécution et par défaut celle-ci est initialisée à 'dev' dans base.py

Les fichiers se trouvent sans **AppliCamions/settings/** et ce dossier contient :
* **base.py** (obligatoire) : paramétrage de base et paramétrage par défaut de l'application. ce fichier est inclus et suivi dans le dépôt. Il définit la variable APP_ENV='dev' par défaut
* ***local.py*** (optionnel) : paramétrage spécifique à l'instance locale. Ce fichier est à créer manuellement et est ignoré par Git. Il permet généralement de modifier la valeur de APP_ENV
Puis selon la valeur de la variable APP_ENV (dev/test/prod) sont chargés les fichiers :
* ***APP_ENV.py*** (optionnel) : paramétrage spécifique à l'instance de l'environnement APP_ENV. Ce fichier est à créer manuellement et s'il existe est à inclure dans le dépôt Git pour qu'il soit commun à tous les environnement APP_ENV
* ***APP_ENV.local.py*** (optionnel) : paramétrage spécifique à l'instance de l'environnement APP_ENV locale. Ce fichier est à créer manuellement et est ignoré par Git

# Développement via Docker

Se référer à la documentation présente dans [le sous-dossier docker du projet](docker/README.md)