from os.path import exists,dirname
from os import write
from .base import *
# Reproduce the Symfony Framework way to manage multiple environment setting files
# By default manage dev/test/prod environments
# Load base.py setting files
# Then try to load local.py if exists. This local.py is generally used to set APP_ENV to test or prod values
# Then load the file according to the APP_ENV value so AP_ENV.py file if exists (default is 'dev' from base.py)
# This APP_ENV.py files are committed to Git
# Then load the APP_ENV.local.py file if exists.
# This APP_ENV.local files are not committed to Git

# load base settings
# then try to load local.py settings
try:
    print('Loading (if exists) ',dirname(__file__),'/local.py', sep='')
    from .local import *
except:
    pass

if APP_ENV == 'dev':
    try:
        print('Loading (if exists) ',dirname(__file__),'/',APP_ENV,'.py', sep='')
        from .dev import *
    except:
        pass
    try:
        print('Loading (if exists) ',dirname(__file__),'/',APP_ENV,'.local','.py', sep='')
        from .dev.local import *
    except:
        pass
if APP_ENV == 'test':
    try:
        print('Loading (if exists) ',dirname(__file__),'/',APP_ENV,'.py', sep='')
        from .test import *
    except:
        pass
    try:
        print('Loading (if exists) ',dirname(__file__),'/',APP_ENV,'.local','.py', sep='')
        from .test.local import *
    except:
        pass
if APP_ENV == 'prod':
    try:
        print('Loading (if exists) ',dirname(__file__),'/',APP_ENV,'.py', sep='')
        from .prod import *
    except:
        pass
    try:
        print('Loading (if exists) ',dirname(__file__),'/',APP_ENV,'.local','.py', sep='')
        from .prod.local import *
    except:
        pass