# Développement via Docker

## Installer docker

Se référer à la [procédure d'installation officielle de Docker](https://docs.docker.com/get-docker/) selon l'environnement 

## Lancer l'aplication

La script \<projet\>/docker/dev.bat (Windows) ou \<projet\>/docker/dev.sh (Linux/TODO) crée un conteneur Docker et monte un volume partagé de la racine du projet vers le répertoire /usr/project/ du conteneur. L'initialisation du conteneur se termine avec le lancement du serveur Django de développement

Si l'initialisation s'est bien déroulée, le site Django du projet est ensuite accessible via l'url : http://localhost:8000

## Ouvrir un terminal de développement au sein du conteneur

Une fois le conteneur de l'application Django lancé, les modifications réalisée au niveau du host au sein du dossier projet impactent directement l'aplication montée au sein du conteneur. Cela est possiblé car à l'initialisation le dossier du projet a été monté et partagé entre l'hôte et le conteneur. Cette configuration est une configuration privilégiée pour le développement mais ne doit pas être utilisée pour de la production

Afin d'ouvrir un terminal au sein du conteneur pour modifier les fichiers et/ou la configuration de Django, deux solutions existent

* se connecter au conteneur via la commande Docker exec
> docker exec -it appli-camions-dev bash
>
> -i : mode interactif<br>
> -t : ouvrir un pseudo-terminal<br>
> nom du conteneur : ici le nom appli-camions-dev<br>
> commande : commande à exécuter, ici bash qui ouvre un terminal bash au sein du conteneur et la session et a l'intérêt de maintenir la session active. Exécuter la commande 'exit' pour terminer la session<br>

* ouvrir un terminal via la script **dev-terminal.bat|sh** 
> \<projet\>/docker/dev-terminal.bat (Windows)<br>
> \<projet\>/docker/dev-terminal.sh (Linux/TODO)<br>
