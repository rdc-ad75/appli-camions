SET PROJECT=%~dp0
docker run --name appli-camions-dev --rm -it -p "8000:8000" -v "%PROJECT%/..:/usr/project" --workdir="/usr/project" python:3.9 bash -c "pip install -r requirements/base.txt && pip install -r requirements/dev.txt && python src/manage.py migrate && python src/manage.py runserver 0.0.0.0:8000" || pause
